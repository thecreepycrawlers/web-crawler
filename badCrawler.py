#!/usr/bin/python2.6 -tt
import urllib
import urlparse
import re
import Queue #our queue
import hashlib
import errno
import os
import sys
import codecs
from bs4 import BeautifulSoup
from socket import error as SocketError

totalLinks = Queue.Queue()
keyLinks = Queue.Queue()
ht = {}

def crawl(url, keyword, depth):
  ht[url] = url
  crawlRec(url,keyword,depth)

def crawlRec(url,keyword, depth):
  try:
    openurl = urllib.urlopen(url)
    content = openurl.read()
    code = openurl.code
  #except SocketError as e:
   # if e.errno != errno.ECONNRESET:
    #  raise
    #pass

  except Exception, e:
    #print '%s: %s\n' % (e, url)
    return
  if code == None:
    openurl.close()
    return
  if code >= 400:
    openurl.close()
    return
  
  soup = BeautifulSoup(content)
  openurl.close()
  
  if soup.body == None:
    return
  
  #if the keyword is found on the current url
  if soup.body.find(text=re.compile(keyword,re.IGNORECASE)):
    keyLinks.enq(url) #add that url to the queue
  if depth == 0: #base case
    return
  # if the keyword is found, every link is sent to listLinks2
  #if soup.body.find(text=re.compile(keyword)):
  #soup.findAll('a', href=True) returns a list of all a tags which have an href
  #this list is iterated through to resolve relative links and to call
  #listLinks2 on each resolved link with the original keyword

  for tag in soup.findAll('a', href=True):
    tag['href'] = urlparse.urljoin(url, tag['href'])
    if isinstance(tag['href'], str):
      tag['href'] = unicode(tag['href'], 'utf-8')
    if tag['href'] not in ht and tag['href']+'/' not in ht:
      ht[tag['href']] = tag['href'] #add it to the hashtable
      print ("HTable size: ", len(ht))
      print ("Queue size: ", q.size())
      
      crawlRec(tag['href'], keyword, depth-1)
  
def main():
  # fdopen adds flush to output. Takes three arguments(file descriptor(
  # what kind of file will be returned), argument indicating how the the
  # file should be opened, files desired buffered size)
  sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  
  UTF8Writer = codecs.getwriter('utf8')  
  sys.stdout = UTF8Writer(sys.stdout)
  
  crawl('http://en.wikipedia.org/wiki/Data_structures', 'hash table', 7)
  print "After crawl"
  q.printQ()
  print "******************"
  for n in ht:
    print n

if __name__ == '__main__':
    main()

# https://pythonhosted.org/kitchen/unicode-frustrations.html
