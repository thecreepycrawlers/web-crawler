#!/usr/bin/python2.6 -tt

import sys
from bs4 import BeautifulSoup
import urllib
import urlparse
import re
import Queue #our queue
import hashlib
import httplib

q = Queue.Queue()
ht = {}

def crawl(url, keyword, depth):
  ht[url] = url
  try:
    openurl = urllib.urlopen(url)
    content = openurl.read()
    code = openurl.code
  except Exception, e:
    print '%s: %s\n' % (e, url)
    return
  if not code/100 >= 4:
    soup = BeautifulSoup(content)
    openurl.close()
    crawlRec(url,soup, keyword, depth-1)
  else:
    openurl.close()
    print "Invalid Link"

def crawlRec(url,soup,keyword,depth):
  if depth == 0: #base case
    return
  
  # delicious soup is made
 # soup = BeautifulSoup(htmllinks)
  # the html file is closed
  #content.close()
  if soup.body == None:
    return
  
  #if the keyword is found on the current url
  if soup.body.find(text=re.compile(keyword,re.IGNORECASE)):
    q.enq(url) #add that url to the queue

    # if the keyword is found, every link is sent to listLinks2
    #if soup.body.find(text=re.compile(keyword)):
    # soup.findAll('a', href=True) returns a list of all a tags which have an href
    #this list is iterated through to resolve relative links and to call
    #listLinks2 on each resolved link with the original keyword
  for tag in soup.findAll('a', href=True):
    tag['href'] = urlparse.urljoin(url, tag['href'])

    #if the link isnt in the hashtable
    if not tag['href'] in ht and not tag['href']+'/' in ht:
      ht[tag['href']] = tag['href'] #add it to the hashtable
      try:
        openurl = urllib.urlopen(tag['href'])
        content = openurl.read()
        code = openurl.code
      except Exception, e:
        print '%s: %s\n' % (e, url)
        return
      if not code/100 >= 4:
        tempSoup = BeautifulSoup(content)
        openurl.close()
        crawlRec(tempSoup, keyword, depth-1) #and crawl it recursively
      else:
        openurl.close()

def main():
  crawl('http://www.pythonforbeginners.com/', 'python', 2)
  q.printQ()
  print "******************"
  for n in ht:
    print n

if __name__ == '__main__':
    main()
