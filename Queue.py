#!/usr/bin/python2.6 -tt

import sys
import hashlib

class Node:
  def __init__(self, val, d=0, n=None):
    self.value = val
    self.depth = d
    self.next = n

class Queue:  
  def __init__(self):
    self.front = None
    self.back = None
    self.length = 0
  
  def enq(self,val, d): # O(1)
    if self.length == 0:
      self.length = 1
      self.front = self.back = Node(val, d) #front is a Node with next node = None
    else:
      self.length += 1
      self.back.next = Node(val, d)
      self.back = self.back.next
  
  def deq(self): # O(1)
    if self.length > 0:
      if self.length == 1:
        self.back = None
      self.length -= 1
      self.front = self.front.next

  def size(self): # O(1) 
    return self.length

  def get_front(self): # O(1)
    if self.length > 0:
      return self.front

  def empty(self):  # O(1)
    self.length == 0    

  def printQ(self): # O(n)
    if self.length > 0:
      n = self.front
      while n != None:
        print n.value
        n = n.next
