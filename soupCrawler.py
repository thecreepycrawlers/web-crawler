#!/usr/bin/python2.6 -tt
import urllib2
import urlparse
import re
from Queue import Queue #our queue
from Queue import Node 
import hashlib
import errno
import os
import sys
import codecs
from bs4 import BeautifulSoup
import socket

def crawl(url, keyword, depth):
  allLinks = Queue()
  keyLinks = Queue()
  ht = {}
  #makes sure user input url is unicode
  if isinstance(url, str):
    url = unicode(url, 'utf-8')

  allLinks.enq(url, 0)
  ht[url] = url

  while allLinks.size() != 0:
    print "HTable Size: ", len(ht)
    print "Queue Size: ", allLinks.size()

    current = allLinks.get_front()
    print "Curr Depth: ", current.depth
    print "Curr Link: ", current.value
    allLinks.deq()
    print "depth: ", depth
    
    try:
      openurl = urllib2.urlopen(current.value)
      content = openurl.read()
      code = openurl.code
    except Exception, e:
      continue
    if code == None or code >= 400 or "<!doctype>" in content or "<!DOCTYPE html>" in content:
      openurl.close()
      continue

    soup = BeautifulSoup(content)
    openurl.close()
    if soup.body == None:
      continue
    if soup.body.find(text=re.compile(keyword, re.IGNORECASE)):
      keyLinks.enq(current.value, current.depth+1)
      
    if current.depth != depth:
      for tag in soup.findAll('a', href=True):
        link = urlparse.urljoin(current.value, tag['href'])
      
        if isinstance(link, str):
         link  = unicode(link, 'utf-8')
      
        if link not in ht and link + unicode('/', 'utf-8') not in ht and not link[0:link.find('#')] in ht :
          ht[link] = link # add to hashtable
          allLinks.enq(link, current.depth + 1) 
  
  print "keyLinks: ", keyLinks.size()
  keyLinks.printQ()

def main():
  
  # fdopen adds flush to output. Takes three arguments(file descriptor(
  # what kind of file will be returned), argument indicating how the the
  # file should be opened, files desired buffered size)
  sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 50)  
  UTF8Writer = codecs.getwriter('utf8')  
  sys.stdout = UTF8Writer(sys.stdout)
  timeout = 5
  socket.setdefaulttimeout(timeout)
  
  sys.settrace(crawl('http://en.wikipedia.org/wiki/Data_structures', 'hash table', 1))

if __name__ == '__main__':
    main()
