#!/bin/bash

rm -f *~ *# *.#
git add --all
git commit -m '$1'
git log --show-notes --pretty="format: %ai %d %s"