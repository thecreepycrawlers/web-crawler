#!/usr/bin/python2.6 -tt
import urllib2
import urlparse
import re
from Queue import Queue #our queue
from Queue import Node 
import hashlib
import errno
import os
import sys
import codecs
import socket
from HTMLParser import HTMLParser
from bs4 import BeautifulSoup

def main():
  try:
    openurl = urllib2.urlopen('https://docs.python.org/2/')
    content = openurl.read()
    code = openurl.code
  except Exception, e:
    pass

  if "<!doctype>" in content or "<!DOCTYPE \"html\">" in content:
    print "Bad doctype"
  soup = BeautifulSoup(content)
  langtag = soup.find('html', lang=True)
  if langtag:
    lang = langtag['lang']
    if 'en' not in lang[0:2]:
      print 'patrick smells'
  
if __name__ == '__main__':
    main()
