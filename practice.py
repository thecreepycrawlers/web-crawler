#!/usr/bin/python2.6 -tt

import sys
from bs4 import BeautifulSoup
import urllib
import urlparse
import re
import Queue #our queue
import hashlib

q = Queue.Queue()
ht = {}

def crawl(url,keyword,depth):
  #if url[-1] != '\':
 #   url = url + '\'
  ht[url]=url
  try:
    content = urllib.urlopen(url)
    htmllinks = content.read()
  except Exception, e:
    print '%s: %s\n' % (e, url)
    return
  # delicious soup is made
  soup = BeautifulSoup(htmllinks)
  # the html file is closed
  content.close()
  if soup.body.find(text=re.compile(keyword)):
    q.enq(url)
  listlinks(url,keyword,depth-1)

def listLinks(url, keyword, depth):
  #print depth
  if depth == 0: #base case
    return
  # Attempts to open the url file
  try:
    content = urllib.urlopen(url)
    htmllinks = content.read()
  except Exception, e:
    print '%s: %s\n' % (e, url)
    return
  # delicious soup is made
  soup = BeautifulSoup(htmllinks)
  # the html file is closed
  content.close()
  if soup.body == None:
    return
  if soup.body.find(text=re.compile(keyword,re.IGNORECASE)):
    q.enq(url)

  # if the keyword is found, every link is sent to listLinks2
  #if soup.body.find(text=re.compile(keyword)):
    # soup.findAll('a', href=True) returns a list of all a tags which have an href
    #this list is iterated through to resolve relative links and to call
    #listLinks2 on each resolved link with the original keyword
  for tag in soup.findAll('a', href=True):
    tag['href'] = urlparse.urljoin(url, tag['href'])
    if tag['href'][-1] != '/':
      tag['href'] += '/'
    if not tag['href'] in ht:
      ht[tag['href']] = tag['href']
    #  print ht      
      listLinks(tag['href'], keyword, depth-1)


def main():
  #result = Queue.Queue()
  listLinks('http://www.pythonforbeginners.com/', 'python', 2)
  q.printQ()
  print "break"
  for n in ht:
    print n
 # print ht
    
if __name__ == '__main__':
    main()
    


#if len(sys.argv) > 1:
     #   for url in sys.argv[1:]: listLinks(url)
    #else: print 'usage: linklister1 url [url2 ...]'
    #Mary()
#parser = MyHTMLParser()
    #f = urllib.urlopen('http://darrenfoley.com')
    #html = f.read()
    #parser.feed(html)
    #parser.close()
    #parser.feed('<html><head><title>Test</title></head>'
               # '<body><h1>Parse me!</h1></body></html>')
#Mary()
  
#if __name__ == '__main__':
 # main()
